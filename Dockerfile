FROM openjdk
RUN mkdir /dl
WORKDIR /dl
ADD https://gitlab.com/BubblegumDiscord/join_sugar_awarder/-/jobs/artifacts/master/download?job=build /dl/artifacts.zip
RUN apt install unzip
RUN unzip artifacts.zip
RUN mkdir /bot
RUN mv build/libs/join_sugar_awarder.jar /bot
WORKDIR /bot
CMD ["java", "-jar", "join_sugar_awarder.jar"]
