# Join Sugar Awarder

Small bot written in Kotlin to award people with sugar when they join Bubblegum

[Download latest artifacts](https://gitlab.com/BubblegumDiscord/join_sugar_awarder/-/jobs/artifacts/master/download?job=build)