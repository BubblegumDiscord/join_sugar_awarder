package voidcrafted
import kotlinx.coroutines.experimental.delay
import sx.blah.discord.api.ClientBuilder
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.api.events.IListener
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.handle.obj.IUser
import kotlinx.coroutines.experimental.launch
import sx.blah.discord.handle.impl.events.ReadyEvent
import org.slf4j.LoggerFactory

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger("JoinSugarAwarder")
    val cb = ClientBuilder()
    val token: String? = System.getenv("AWARDER_TOKEN")
    if (token == null) {
        println("Token in AWARDER_TOKEN env required")
        System.exit(1)
    }
    cb.withToken(token)
    logger.info("Logging in")
    val client: IDiscordClient = cb.login()
    logger.info("Logged in as " + client.applicationClientID)
    var welcomeChannel: IChannel? = null
    client.dispatcher.registerListener(IListener<ReadyEvent> { event: ReadyEvent? ->
        welcomeChannel = client.getChannelByID(457220496224944129L)
        logger.info("Retrieved #" + welcomeChannel?.name)
    })
    client.dispatcher.registerListener(IListener<UserJoinEvent> { event ->
        // We can be sure it's not null so we !! it, as we couldn't
        // have received the event if we are not currently ready
        var defoAChannel: IChannel = welcomeChannel!!

        // Log it
        logger.info("Person ${event.user.name}#${event.user.discriminator} joined (${event.user.stringID})")
        var userInvolved: IUser = event.user
        launch {
            defoAChannel.sendMessage("\$award 1 ${userInvolved.stringID}")
            delay(2000)
            defoAChannel.sendMessage("\$execsql UPDATE DiscordUser SET CurrencyAmount=0 WHERE UserId=${userInvolved.stringID}")
            delay(2000)
            defoAChannel.sendMessage("yes")
            delay(2000)
            defoAChannel.sendMessage("\$award 25000 ${userInvolved.stringID}")
            logger.info("Finished awarding ${event.user.stringID}")
        }
    })
}
